import {seeddistribution,nbseed} from "./btninteraction.js"

// array player
let random = ["player1","player2"]

//randomly generator of a number to choose a player
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

//modify style if it's player turn linked to the function swithplayer
function modifystyle(id,col){

    let listliplayer = document.getElementsByClassName(id)

        for (let index = 0; index < listliplayer.length; index++) {
            
            if(col == "red"){

                listliplayer.item(index).style.color = col
                listliplayer.item(index).style.cursor = "pointer" 

            }else if(col == "blue"){

                listliplayer.item(index).style.color = col
                listliplayer.item(index).style.cursor = "pointer" 

            }else if(col == "grey"){

                listliplayer.item(index).style.color = col
                listliplayer.item(index).style.cursor = "not-allowed"  
            }
            
        }

}

//disable player's granaries when it is not his turn linked to the function swithplayer
function disabledplayer(id){

    let listliplayerop = document.getElementsByClassName(id)

    for (let index = 0; index < listliplayerop.length; index++) {
            
        listliplayerop.item(index).removeEventListener("click",function(e){
            e.stopPropagation()
        },true)
            
    }

}

// read my table (for player 1 or player 2) from begin to end and define border color 
//and color for each player and to prohibit click on the other side 
function switchplayer(player_id,color){

    if(player_id == "player1"){

        modifystyle(player_id,color)
        
        disabledplayer("player2")

    }else{

        modifystyle(player_id,color)

        disabledplayer("player1")

    }
   
}

// 2 cases : the player1 begins so color of the player1 game side change into red;
//           and the message changed and the player is indicated.
//           the player2 beings so color changes into blue.
export function whichplayerbegin(player_id){

    switch (player_id) {
        case "player1":
            document.getElementById(player_id).style.color = "red"
            switchplayer(player_id,"red")
            document.getElementById("textstart").innerHTML = "It's your turn,Player 1"
            document.getElementById("player2").style.color = "grey"
            switchplayer("player2","grey")
            break
    
        case "player2":
            document.getElementById(player_id).style.color = "blue" 
            switchplayer(player_id,"blue")
            document.getElementById("textstart").innerHTML = "It's your turn,Player 2"
            document.getElementById("player1").style.color = "grey"
            switchplayer("player1","grey")
            break
    }

}

//choose which player begin first
function choosewhobegin(tabrandom){

    let result = tabrandom[getRandomInt(2)]
    
    whichplayerbegin(result)
    
    return result
}

export function gameInit()
{

    document.addEventListener("DOMContentLoaded",function(){  
    
        let player = choosewhobegin(random)
    
        seeddistribution(player)
        
    })
}

gameInit()

