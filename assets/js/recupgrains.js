import {gameInit} from "./global.js"

function refreshgame(){
    gameInit()
}

function winner(player)
{
    let finalscore = parseInt(document.getElementById(player+"-score").innerHTML)
    if(finalscore >= 25){
        let player_name = (player == "player1") ? "Player 1" : "Player 2"
        alert(player_name + " wins with "+finalscore+" points")
    }
}

function scoring(id,grains,player,score,lasthole)
{
    let ids = (player=="player1") ? 6 : 0
    let arrscore = [0,0]
    let index = (player=="player1") ? 0 : 1

    if(grains.className != player){

        while(id != ids){

            let grainsrecup = grains.parentElement.querySelector(`#${CSS.escape(id.toString())}`)
            let nbrgrains = parseInt(grainsrecup.innerHTML)

            if(grainsrecup.id == lasthole){
    
                if((nbrgrains == 2 || nbrgrains == 3) && grainsrecup.className != player){

                    alert("nbrgrains : "+nbrgrains)
                    score += nbrgrains
                    

                    //alert("lasthole : "+lasthole)
                    alert("grainsrecup.id : "+grainsrecup.id)

                    grainsrecup.innerHTML = 0
                    lasthole--
                }
    
            }
            
            id--
        }
        arrscore[index] += score
        alert("score "+player+" : "+arrscore[index])
    }

    document.getElementById(player+"-score").innerHTML = parseInt(document.getElementById(player+"-score").innerHTML) + arrscore[index]

    winner(player)

}

export function recupgrains(player, lastHole){

    //if the lastHole is in the adverse side, my functin begins : 
    let grains = document.getElementById(lastHole)
    let id,score = 0

    id = parseInt(grains.id)

    scoring(id,grains,player,score,lastHole)

}
